﻿namespace employeePaySystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.name1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rbSalaried = new System.Windows.Forms.RadioButton();
            this.rbHourlyRate = new System.Windows.Forms.RadioButton();
            this.hoursWorkedText = new System.Windows.Forms.TextBox();
            this.salaryText = new System.Windows.Forms.TextBox();
            this.hourlyPayText = new System.Windows.Forms.TextBox();
            this.salariedRate = new System.Windows.Forms.Label();
            this.hoursWorked = new System.Windows.Forms.Label();
            this.hourlyPay = new System.Windows.Forms.Label();
            this.submit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(164, 12);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(197, 20);
            this.textBoxFirstName.TabIndex = 0;
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(164, 64);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(197, 20);
            this.textBoxId.TabIndex = 4;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(164, 38);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(197, 20);
            this.textBoxLastName.TabIndex = 2;
            // 
            // name1
            // 
            this.name1.AutoSize = true;
            this.name1.Location = new System.Drawing.Point(39, 19);
            this.name1.Name = "name1";
            this.name1.Size = new System.Drawing.Size(106, 13);
            this.name1.TabIndex = 6;
            this.name1.Text = "Employee First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Employee Last name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Employee ID";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // rbSalaried
            // 
            this.rbSalaried.AutoSize = true;
            this.rbSalaried.Location = new System.Drawing.Point(285, 90);
            this.rbSalaried.Name = "rbSalaried";
            this.rbSalaried.Size = new System.Drawing.Size(63, 17);
            this.rbSalaried.TabIndex = 8;
            this.rbSalaried.TabStop = true;
            this.rbSalaried.Text = "Salaried";
            this.rbSalaried.UseVisualStyleBackColor = true;
            this.rbSalaried.CheckedChanged += new System.EventHandler(this.rbSalaried_CheckedChanged);
            // 
            // rbHourlyRate
            // 
            this.rbHourlyRate.AutoSize = true;
            this.rbHourlyRate.Location = new System.Drawing.Point(164, 90);
            this.rbHourlyRate.Name = "rbHourlyRate";
            this.rbHourlyRate.Size = new System.Drawing.Size(76, 17);
            this.rbHourlyRate.TabIndex = 6;
            this.rbHourlyRate.TabStop = true;
            this.rbHourlyRate.Text = "hourlyRate";
            this.rbHourlyRate.UseVisualStyleBackColor = true;
            this.rbHourlyRate.CheckedChanged += new System.EventHandler(this.rbHourlyRate_CheckedChanged);
            // 
            // hoursWorkedText
            // 
            this.hoursWorkedText.Location = new System.Drawing.Point(106, 128);
            this.hoursWorkedText.Name = "hoursWorkedText";
            this.hoursWorkedText.Size = new System.Drawing.Size(100, 20);
            this.hoursWorkedText.TabIndex = 12;
            this.hoursWorkedText.Visible = false;
            // 
            // salaryText
            // 
            this.salaryText.Location = new System.Drawing.Point(260, 125);
            this.salaryText.Name = "salaryText";
            this.salaryText.Size = new System.Drawing.Size(100, 20);
            this.salaryText.TabIndex = 16;
            this.salaryText.Visible = false;
            this.salaryText.TextChanged += new System.EventHandler(this.salaryText_TextChanged);
            // 
            // hourlyPayText
            // 
            this.hourlyPayText.Location = new System.Drawing.Point(106, 154);
            this.hourlyPayText.Name = "hourlyPayText";
            this.hourlyPayText.Size = new System.Drawing.Size(100, 20);
            this.hourlyPayText.TabIndex = 14;
            this.hourlyPayText.Visible = false;
            // 
            // salariedRate
            // 
            this.salariedRate.AutoSize = true;
            this.salariedRate.Location = new System.Drawing.Point(212, 125);
            this.salariedRate.Name = "salariedRate";
            this.salariedRate.Size = new System.Drawing.Size(36, 13);
            this.salariedRate.TabIndex = 16;
            this.salariedRate.Text = "Salary";
            // 
            // hoursWorked
            // 
            this.hoursWorked.AutoSize = true;
            this.hoursWorked.Location = new System.Drawing.Point(12, 128);
            this.hoursWorked.Name = "hoursWorked";
            this.hoursWorked.Size = new System.Drawing.Size(76, 13);
            this.hoursWorked.TabIndex = 17;
            this.hoursWorked.Text = "Hours Worked";
            // 
            // hourlyPay
            // 
            this.hourlyPay.AutoSize = true;
            this.hourlyPay.Location = new System.Drawing.Point(15, 160);
            this.hourlyPay.Name = "hourlyPay";
            this.hourlyPay.Size = new System.Drawing.Size(58, 13);
            this.hourlyPay.TabIndex = 18;
            this.hourlyPay.Text = "Hourly Pay";
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(273, 155);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(75, 23);
            this.submit.TabIndex = 18;
            this.submit.Text = "submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 204);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.hourlyPay);
            this.Controls.Add(this.hoursWorked);
            this.Controls.Add(this.salariedRate);
            this.Controls.Add(this.hourlyPayText);
            this.Controls.Add(this.salaryText);
            this.Controls.Add(this.hoursWorkedText);
            this.Controls.Add(this.rbHourlyRate);
            this.Controls.Add(this.rbSalaried);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name1);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.textBoxFirstName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label name1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbSalaried;
        private System.Windows.Forms.RadioButton rbHourlyRate;
        private System.Windows.Forms.TextBox hoursWorkedText;
        private System.Windows.Forms.TextBox salaryText;
        private System.Windows.Forms.TextBox hourlyPayText;
        private System.Windows.Forms.Label salariedRate;
        private System.Windows.Forms.Label hoursWorked;
        private System.Windows.Forms.Label hourlyPay;
        private System.Windows.Forms.Button submit;
    }
}

