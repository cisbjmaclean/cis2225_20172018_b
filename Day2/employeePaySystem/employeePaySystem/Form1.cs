﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace employeePaySystem
{
    public partial class Form1 : Form
    {
        const double fedTax = 0.18;
        const double retirementPlan = 0.10;
        const double socialSecurity = 0.06;
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void rbSalaried_CheckedChanged(object sender, EventArgs e)
        {
            //If the user selects the salaried rate as a pay option, make the input boxes 
            //visible for salary pay, and the input options for hourly pay invisible.
            salaryText.Visible = true;
            salariedRate.Visible = true;

            hoursWorkedText.Visible = false;
            hoursWorked.Visible = false;
            hourlyPay.Visible = false;
            hourlyPayText.Visible = false;
        }


        private void rbHourlyRate_CheckedChanged(object sender, EventArgs e)
        {   
            //If the user selects the hourly rate as a pay option, make the input boxes 
            //visible for hourly pay, and the input options for salary pay invisible.
            hoursWorkedText.Visible = true;
            hoursWorked.Visible = true;
            hourlyPay.Visible = true;
            hourlyPayText.Visible = true;

            salaryText.Visible = false;
            salariedRate.Visible = false;
        }

        private void salaryText_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void submit_Click(object sender, EventArgs e)
        {



            double salary=0;
            double deductables;
            double total=0;
            bool validType = true;

            //Checking to see if the salary has been set, if not check for hourly pay.
            if (rbSalaried.Checked)
            {
                double.TryParse(salaryText.Text, out salary);
                salary /= 52;

            }//Checking to see if the hourly pay has been set.
            else if (rbHourlyRate.Checked)
            {
                int hoursWorkedInt;
                int.TryParse(hoursWorkedText.Text,out hoursWorkedInt);
                int hourlyRateInt;
                int.TryParse(hourlyPayText.Text,out hourlyRateInt);
                if (hoursWorkedInt > 40)
                {
                    salary = (40 * hourlyRateInt) + (hoursWorkedInt - 40) * 1.5 * hourlyRateInt;
                }
                else
                {
                    salary = (hoursWorkedInt * hourlyRateInt);
                }

            }
            else//If no pay option was selected, notify the user and ask them to do so.
            {
                validType = false;
            }


            //Switch statement
            /*            switch (validType)
            {
                case true:
                  deductables = (salary * retirementPlan);
                  deductables += (salary * socialSecurity);
                  total = salary - deductables;
                  MessageBox.Show("" + total);
                  break;
            }
            */

            //if statement
            /*
            if (validType)
            
            {
                deductables = (salary * retirementPlan);
                deductables += (salary * socialSecurity);
                total = salary - deductables;
                MessageBox.Show("" + total);
            }
            */
            //Otherwise can use a ternary operator
            String output = validType ?  "Net: "+String.Format("{0:C}",total) : "Enter type";
            MessageBox.Show(output);

        }
    }
}
