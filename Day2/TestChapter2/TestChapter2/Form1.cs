﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestChapter2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBoxEntry.Text = "99";
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            String entry = textBoxEntry.Text;
            MessageBox.Show(string.Format("{0:c}",double.Parse(entry)));

            //or

            decimal tempDecimal = decimal.Parse(entry);
            MessageBox.Show(tempDecimal.ToString());


        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBoxEntry_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Enter key is down

                //Capture the text
                if (sender is TextBox)
                {
                    TextBox txb = (TextBox)sender;
                    MessageBox.Show(txb.Text);
                }
            }
        }
    }
}
