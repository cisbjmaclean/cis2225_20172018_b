﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
Progam name: Login
Version: # 1
Description: This control will be the standard login control for CIS2225
Change History: 2017.11.13 Original version by BJM
*/

namespace WindowsFormsControlLibraryCIS2225
{
    public partial class UserControlLogin: UserControl
    {
        public UserControlLogin()
        {
            InitializeComponent();
            tlTpLogin.SetToolTip(txtBxUsername, "Please enter username");
            tlTpLogin.SetToolTip(txtBxPassword, "Please enter password");
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            MessageBox.Show("User is loggin in");
        }
    }
}
