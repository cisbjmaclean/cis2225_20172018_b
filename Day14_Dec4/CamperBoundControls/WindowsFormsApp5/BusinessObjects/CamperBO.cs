﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp5.DatabaseObjects;

namespace WindowsFormsApp5.BusinessObjects
{
    class CamperBO
    {
        public static System.Data.DataSet getCamperDataSet()
        {
            CamperDAO camperDAO = new CamperDAO();
            return camperDAO.getDataSet();
        }

        public static void updateCamperDataSet(System.Data.DataSet dataSet)
        {
            CamperDAO camperDAO = new CamperDAO();
            camperDAO.updateDataSet(dataSet);
        }
    }
}