﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp5.BusinessObjects;

namespace WindowsFormsApp5
{
    public partial class Form1 : Form
    {
        private DataSet camperDataSet;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            camperDataSet = CamperBO.getCamperDataSet();
            dataGridView1.DataSource = camperDataSet;
            dataGridView1.DataMember = "Camper";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            CamperBO.updateCamperDataSet(camperDataSet);
        }
    }
}
