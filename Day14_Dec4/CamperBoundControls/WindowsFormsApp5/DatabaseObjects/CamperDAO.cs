﻿using System.Data;
using System.Data.OleDb;

namespace WindowsFormsApp5.DatabaseObjects
{
    class CamperDAO
    {
        //From page 914
        private OleDbConnection dbConn;
        private OleDbCommand dbCmd;
        private string sConnection;
        private string sql;
        private OleDbDataAdapter camperDataAdap;
        private DataSet dataSet;
        private OleDbCommandBuilder cBuilder;

        public void updateDataSet(DataSet camperDataSet)
        {
            //: //Colon indicates items missing
            sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" +
        "Data Source=Canes.accdb";
            dbConn = new OleDbConnection(sConnection);

            sql = "SELECT * from camper Order BY lastName ASC, firstName ASC;";
            dbCmd = new OleDbCommand();
            dbCmd.CommandText = sql;
            dbCmd.Connection = dbConn;

            camperDataAdap = new OleDbDataAdapter();
            camperDataAdap.SelectCommand = dbCmd;
            cBuilder = new OleDbCommandBuilder(camperDataAdap);
            camperDataAdap.Update(camperDataSet, "Camper");
        }

        public DataSet getDataSet()
        {
            //: //Colon indicates items missing
            sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" +
        "Data Source=Canes.accdb";
            dbConn = new OleDbConnection(sConnection);

            sql = "SELECT * from camper Order BY lastName ASC, firstName ASC;";
            dbCmd = new OleDbCommand();
            dbCmd.CommandText = sql;
            dbCmd.Connection = dbConn;

            camperDataAdap = new OleDbDataAdapter();
            camperDataAdap.SelectCommand = dbCmd;

            dataSet = new DataSet();
            camperDataAdap.Fill(dataSet, "Camper");
            return dataSet;


        }

    }
}