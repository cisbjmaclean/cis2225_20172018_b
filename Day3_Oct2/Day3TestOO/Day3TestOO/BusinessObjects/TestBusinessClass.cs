﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Day3TestOO.BusinessObjects
{
    class TestBusinessClass
    {
        public static void Test(string name)
        {
            MessageBox.Show("Hello" + name);
            Console.WriteLine("Hello" + name);

            //Create a student
            Student student = new Student();

            //Use property to set the student id
            student.StudentId = 3;

            //Use the setter to set the name
            student.SetName("Bob");

            Console.WriteLine("Here's the guy: " + student.ToString());
        }
    
    
    }
}
