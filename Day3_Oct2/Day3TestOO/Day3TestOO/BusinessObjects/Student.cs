﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3TestOO.BusinessObjects
{
    class Student
    {
        private String name;
        private int studentId;

        public void SetName(string inName)
        {
            this.name = inName;
        }

        public int StudentId
        {
            get
            {
                return studentId;
            }
            set
            {
                studentId = value;
            }
        }

        public String ToString()
        {
            string output = "Name: " + name + " id=" + studentId;
            return output;
        }
    }
}
