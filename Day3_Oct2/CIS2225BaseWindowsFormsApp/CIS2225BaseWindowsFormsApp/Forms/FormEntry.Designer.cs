﻿namespace CIS2225BaseWindowsFormsApp.Forms
{
    partial class windowTestHover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestHover2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTestHover2
            // 
            this.btnTestHover2.Location = new System.Drawing.Point(122, 85);
            this.btnTestHover2.Name = "btnTestHover2";
            this.btnTestHover2.Size = new System.Drawing.Size(209, 100);
            this.btnTestHover2.TabIndex = 0;
            this.btnTestHover2.Text = "Test Hover";
            this.btnTestHover2.UseVisualStyleBackColor = true;
            this.btnTestHover2.Click += new System.EventHandler(this.btnTestHover2_Click);
            // 
            // windowTestHover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 322);
            this.Controls.Add(this.btnTestHover2);
            this.Name = "windowTestHover";
            this.Text = "Test Hover";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTestHover2;

    }
}