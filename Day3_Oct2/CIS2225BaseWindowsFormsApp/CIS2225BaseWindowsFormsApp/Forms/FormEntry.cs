﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIS2225BaseWindowsFormsApp.Forms
{
    public partial class windowTestHover : FormBase
    {
        public windowTestHover()
        {
            InitializeComponent();
        }

        private void btnTestHover2_MouseHover(object sender, EventArgs e)
        {
            btnTestHover2.Cursor = Cursors.Hand;
           Console.WriteLine("entered with mouse");
        }

        private void btnTestHover2_MouseLeave(object sender, EventArgs e)
        {
            btnTestHover2.Cursor = Cursors.Default;
            Console.WriteLine("left with mouse");
        }

        private void btnTestHover2_Click(object sender, EventArgs e)
        {
            btnTestHover2.Cursor = Cursors.No;
            Console.WriteLine("clicked with mouse");

        }

    }
}
