﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest1_vs2017
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabControlPayType.SelectTab(tabPageHourly);
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            MessageBox.Show(maskedTextBox1.Text);

            if (tabControlPayType.SelectedTab == tabPageSalary)
            {
                MessageBox.Show("Salary");
            }
            if (tabControlPayType.SelectedTab == tabPageHourly)
            {
                MessageBox.Show("Hourly");
            }
        }
    }
}
