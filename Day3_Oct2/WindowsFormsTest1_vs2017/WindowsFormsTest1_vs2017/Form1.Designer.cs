﻿namespace WindowsFormsTest1_vs2017
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.tabControlPayType = new System.Windows.Forms.TabControl();
            this.tabPageSalary = new System.Windows.Forms.TabPage();
            this.tabPageHourly = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.tabControlPayType.SuspendLayout();
            this.tabPageHourly.SuspendLayout();
            this.SuspendLayout();
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(14, 173);
            this.maskedTextBox1.Margin = new System.Windows.Forms.Padding(1);
            this.maskedTextBox1.Mask = "000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(94, 20);
            this.maskedTextBox1.TabIndex = 0;
            // 
            // tabControlPayType
            // 
            this.tabControlPayType.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlPayType.Controls.Add(this.tabPageSalary);
            this.tabControlPayType.Controls.Add(this.tabPageHourly);
            this.tabControlPayType.Location = new System.Drawing.Point(10, 6);
            this.tabControlPayType.Margin = new System.Windows.Forms.Padding(1);
            this.tabControlPayType.Name = "tabControlPayType";
            this.tabControlPayType.SelectedIndex = 0;
            this.tabControlPayType.Size = new System.Drawing.Size(293, 159);
            this.tabControlPayType.TabIndex = 1;
            // 
            // tabPageSalary
            // 
            this.tabPageSalary.Location = new System.Drawing.Point(4, 25);
            this.tabPageSalary.Margin = new System.Windows.Forms.Padding(1);
            this.tabPageSalary.Name = "tabPageSalary";
            this.tabPageSalary.Padding = new System.Windows.Forms.Padding(1);
            this.tabPageSalary.Size = new System.Drawing.Size(285, 130);
            this.tabPageSalary.TabIndex = 0;
            this.tabPageSalary.Text = "Salary";
            this.tabPageSalary.UseVisualStyleBackColor = true;
            this.tabPageSalary.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPageHourly
            // 
            this.tabPageHourly.Controls.Add(this.label1);
            this.tabPageHourly.Controls.Add(this.textBox1);
            this.tabPageHourly.Location = new System.Drawing.Point(4, 25);
            this.tabPageHourly.Margin = new System.Windows.Forms.Padding(1);
            this.tabPageHourly.Name = "tabPageHourly";
            this.tabPageHourly.Padding = new System.Windows.Forms.Padding(1);
            this.tabPageHourly.Size = new System.Drawing.Size(285, 130);
            this.tabPageHourly.TabIndex = 1;
            this.tabPageHourly.Text = "Hourly";
            this.tabPageHourly.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "hours";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(81, 17);
            this.textBox1.Margin = new System.Windows.Forms.Padding(1);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(45, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(214, 169);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 2;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 221);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.tabControlPayType);
            this.Controls.Add(this.maskedTextBox1);
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControlPayType.ResumeLayout(false);
            this.tabPageHourly.ResumeLayout(false);
            this.tabPageHourly.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.TabControl tabControlPayType;
        private System.Windows.Forms.TabPage tabPageSalary;
        private System.Windows.Forms.TabPage tabPageHourly;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnCalculate;
    }
}

