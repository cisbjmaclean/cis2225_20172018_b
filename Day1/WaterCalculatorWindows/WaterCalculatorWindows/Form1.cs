﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaterCalculatorWindows
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            if (checkBoxTest.Checked)
            {

                MessageBox.Show(checkBoxTest.Checked.ToString(), "Test");
            }
            else
            {
                MessageBox.Show("Not checked", "Test");
            }
            calculate();
        }

        private void calculate()
        {
            double diameter;
            if (!double.TryParse(textBoxDiameter.Text, out diameter))
            {
                //
            }

            double height;
            if (!double.TryParse(textBoxHeight.Text, out height))
            {
            }

            //Calculate the volume
            double volume = Math.PI * Math.Pow((diameter / 2.0), 2) * (height);


            double flowrate;
            if (!double.TryParse(textBoxFlowRate.Text, out flowrate))
            {
            }

            const double CUBIC_INCHES_PER_LITER = 61.0;
            double time = volume * (flowrate / CUBIC_INCHES_PER_LITER);

            //Set the output.
            textBoxTime.Text = Convert.ToString(time);
        }

    }
}
