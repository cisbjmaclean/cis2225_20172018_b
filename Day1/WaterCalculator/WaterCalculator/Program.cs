﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Water calculator which will determine length of time 
//to fill a cylander.
namespace WaterCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the diameter in inches");
            double diameter;
            if (!double.TryParse(Console.ReadLine(), out diameter))
            {
                Console.WriteLine("Error with diameter");
                Console.ReadKey();
            }

            Console.WriteLine("Enter the height in inches");
            double height;
            if (!double.TryParse(Console.ReadLine(), out height))
            {
                Console.WriteLine("Error with height");
                Console.ReadKey();
            }
            
            //Calculate the volume
            double volume = Math.PI * Math.Pow((diameter / 2.0),2) * (height);

            Console.WriteLine("The volume is " + volume + " cubic inches");

            Console.WriteLine("Enter the flow rate (seconds/liter)");
            double flowrate;
            if (!double.TryParse(Console.ReadLine(), out flowrate))
            {
                Console.WriteLine("Error with flow rate");
                Console.ReadKey();
            }

            const double CUBIC_INCHES_PER_LITER = 61.0;
            double time = volume * (flowrate / CUBIC_INCHES_PER_LITER);

            Console.WriteLine("It would take " + time + " seconds to fill the container");
            Console.ReadKey();




        }
    }
}
