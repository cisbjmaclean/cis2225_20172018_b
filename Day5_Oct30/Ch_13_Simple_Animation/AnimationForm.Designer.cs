namespace Ch13SimpleAnimation
{
    partial class AnimationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.changePictureButton = new System.Windows.Forms.Button();
            this.displayPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.displayPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // changePictureButton
            // 
            this.changePictureButton.Location = new System.Drawing.Point(72, 123);
            this.changePictureButton.Name = "changePictureButton";
            this.changePictureButton.Size = new System.Drawing.Size(75, 23);
            this.changePictureButton.TabIndex = 1;
            this.changePictureButton.Text = "&Change";
            this.changePictureButton.UseVisualStyleBackColor = true;
            this.changePictureButton.Click += new System.EventHandler(this.changePictureButton_Click);
            // 
            // displayPictureBox
            // 
            this.displayPictureBox.InitialImage = global::Ch13SimpleAnimation.Properties.Resources.PreviousPage;
            this.displayPictureBox.Location = new System.Drawing.Point(86, 44);
            this.displayPictureBox.Name = "displayPictureBox";
            this.displayPictureBox.Size = new System.Drawing.Size(37, 39);
            this.displayPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.displayPictureBox.TabIndex = 0;
            this.displayPictureBox.TabStop = false;
            // 
            // AnimationForm
            // 
            this.AcceptButton = this.changePictureButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 213);
            this.Controls.Add(this.changePictureButton);
            this.Controls.Add(this.displayPictureBox);
            this.Name = "AnimationForm";
            this.Text = "Simple Animation";
            this.Load += new System.EventHandler(this.AnimationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.displayPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox displayPictureBox;
        private System.Windows.Forms.Button changePictureButton;
    }
}

