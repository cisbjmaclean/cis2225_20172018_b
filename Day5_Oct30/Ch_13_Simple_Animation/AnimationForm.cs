/* Program:     Ch13SimpleAnimation  
 * Programmer:  Bradley/Millspaugh
 * Date:        June 2007
 * Description: Change one picture to another.
 *              This project uses the graphics in the VS2005ImageLibrary and
 *              makes the magenta background transparent.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ch13SimpleAnimation
{
    public partial class AnimationForm : Form
    {
        public AnimationForm()
        {
            InitializeComponent();
        }

        bool switchBoolean = true;
        Bitmap nextPageBitmap;
        Bitmap previousPageBitmap;

        private void changePictureButton_Click(object sender, EventArgs e)
        {
            // Switch the picture.

            if (switchBoolean)
            {
                displayPictureBox.Image = previousPageBitmap;
                switchBoolean = false;
            }
            else
            {
                displayPictureBox.Image = nextPageBitmap;
                switchBoolean = true;
            }
        }

        private void AnimationForm_Load(object sender, EventArgs e)
        {
            // Set up the transparent images.

            nextPageBitmap = Ch13SimpleAnimation.Properties.Resources.NextPage;
            nextPageBitmap.MakeTransparent(Color.Magenta);
            previousPageBitmap = Ch13SimpleAnimation.Properties.Resources.PreviousPage;
            previousPageBitmap.MakeTransparent(Color.Magenta);
            displayPictureBox.Image = nextPageBitmap;
        }
    }
}