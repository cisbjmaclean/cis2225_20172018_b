﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDIDemo.Forms
{
    public partial class frmChild : Form
    {
        public frmChild()
        {
            InitializeComponent();
        }
        public string fileName = "";

        private void frmChild_Load(object sender, EventArgs e)
        {
            if (fileName != "")
            {
                rtxNotes.LoadFile(fileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sfdSave.DefaultExt = "*.rtf";
            sfdSave.Filter = "RTF Files|" + sfdSave.DefaultExt;

            if (sfdSave.ShowDialog() == System.Windows.Forms.DialogResult.OK && sfdSave.FileName.Length > 0)
            {
                rtxNotes.SaveFile(sfdSave.FileName, RichTextBoxStreamType.PlainText);
            }
        }
    }
}
