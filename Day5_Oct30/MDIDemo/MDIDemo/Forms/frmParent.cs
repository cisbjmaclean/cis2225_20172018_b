﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDIDemo
{
    public partial class frmParent : Form
    {
        public frmParent()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forms.frmChild newMDIChild = new Forms.frmChild();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form.  
            newMDIChild.Text = "Child1";
            newMDIChild.Show();  
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ofdOpenFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //MessageBox.Show("name of the file " + ofdOpenFile.FileName);
                Forms.frmChild newMDIChild = new Forms.frmChild();
                // Set the Parent Form of the Child window.  
                newMDIChild.MdiParent = this;
                // Display the new form.  
                newMDIChild.Text = "Child1";
                newMDIChild.fileName = ofdOpenFile.FileName;
                newMDIChild.Show();  
            }
        }
    }
}
