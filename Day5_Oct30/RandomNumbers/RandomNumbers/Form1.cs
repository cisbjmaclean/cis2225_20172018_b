﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomNumbers
{
    public partial class Form1 : Form
    {
        Random generateRandom = new Random();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //generate dots in random locations
            //draw a snowman at the bottom center of the screen
            int xInteger = Convert.ToInt32(this.Width / 2),
            yInteger = Convert.ToInt32(this.Height / 2);
            Pen whitePen = new Pen(Color.White, 2);

            //draw the snowman
            e.Graphics.FillEllipse(Brushes.White, xInteger, yInteger, 100, 100);
            //top of last circle
            yInteger -= 80;
            //offset for smaller circle
            xInteger += 10;
            e.Graphics.FillEllipse(Brushes.White, xInteger, yInteger, 80, 80);
            yInteger -= 60;
            xInteger += 8;
            e.Graphics.FillEllipse(Brushes.White, xInteger, yInteger, 60, 60);

            //add a top hat
            e.Graphics.DrawLine(Pens.Black, xInteger - 10, yInteger, xInteger + 80, yInteger);
            e.Graphics.FillRectangle(Brushes.Black, xInteger + 10, yInteger - 40, 40, 40);

            //make it snow in random locations
            for (int indexInteger = 1; indexInteger < 40000; indexInteger++)
            {
                xInteger = generateRandom.Next(1, this.Width);
                yInteger = generateRandom.Next(1, this.Height);
                e.Graphics.DrawLine(whitePen, xInteger, yInteger, xInteger + 1, yInteger + 1);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
