﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace October30ControlsDemo
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is where you would put code to save form");
        }

        private void homeProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnGetDate_Click(object sender, EventArgs e)
        {
            string date = dtpExample.Value.ToShortDateString();
            //string theDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            MessageBox.Show("This is the value selected " + date);
        }
    }
}
