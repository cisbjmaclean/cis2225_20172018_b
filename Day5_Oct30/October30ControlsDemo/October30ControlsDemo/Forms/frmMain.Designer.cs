﻿namespace October30ControlsDemo
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.cmsSave = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homeProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipGeneric = new System.Windows.Forms.ToolTip(this.components);
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtBigMessage = new System.Windows.Forms.TextBox();
            this.dtpExample = new System.Windows.Forms.DateTimePicker();
            this.btnGetDate = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.cmsSave.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFirstName
            // 
            this.txtFirstName.ContextMenuStrip = this.cmsSave;
            this.txtFirstName.Location = new System.Drawing.Point(133, 70);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(100, 20);
            this.txtFirstName.TabIndex = 0;
            this.tipGeneric.SetToolTip(this.txtFirstName, "Enter First Name\r\nThis is the second line\r\n*****\r\n");
            // 
            // cmsSave
            // 
            this.cmsSave.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.newToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.cmsSave.Name = "cmsSave";
            this.cmsSave.Size = new System.Drawing.Size(100, 92);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeProjectToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // homeProjectToolStripMenuItem
            // 
            this.homeProjectToolStripMenuItem.Name = "homeProjectToolStripMenuItem";
            this.homeProjectToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.homeProjectToolStripMenuItem.Text = "Home Project";
            this.homeProjectToolStripMenuItem.Click += new System.EventHandler(this.homeProjectToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // tipGeneric
            // 
            this.tipGeneric.IsBalloon = true;
            this.tipGeneric.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(133, 112);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(100, 20);
            this.txtLastName.TabIndex = 1;
            this.tipGeneric.SetToolTip(this.txtLastName, "Enter Last Name");
            // 
            // txtBigMessage
            // 
            this.txtBigMessage.Location = new System.Drawing.Point(133, 155);
            this.txtBigMessage.Name = "txtBigMessage";
            this.txtBigMessage.Size = new System.Drawing.Size(100, 20);
            this.txtBigMessage.TabIndex = 2;
            this.tipGeneric.SetToolTip(this.txtBigMessage, "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff" +
        "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
            // 
            // dtpExample
            // 
            this.dtpExample.CustomFormat = "";
            this.dtpExample.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExample.Location = new System.Drawing.Point(36, 198);
            this.dtpExample.Name = "dtpExample";
            this.dtpExample.Size = new System.Drawing.Size(150, 20);
            this.dtpExample.TabIndex = 3;
            this.dtpExample.Value = new System.DateTime(2017, 10, 30, 0, 0, 0, 0);
            // 
            // btnGetDate
            // 
            this.btnGetDate.Location = new System.Drawing.Point(36, 226);
            this.btnGetDate.Name = "btnGetDate";
            this.btnGetDate.Size = new System.Drawing.Size(75, 23);
            this.btnGetDate.TabIndex = 4;
            this.btnGetDate.Text = "Get Date";
            this.btnGetDate.UseVisualStyleBackColor = true;
            this.btnGetDate.Click += new System.EventHandler(this.btnGetDate_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ContextMenuStrip = this.cmsSave;
            this.Controls.Add(this.btnGetDate);
            this.Controls.Add(this.dtpExample);
            this.Controls.Add(this.txtBigMessage);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtFirstName);
            this.Name = "frmMain";
            this.Text = "Main Form";
            this.tipGeneric.SetToolTip(this, "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
            this.cmsSave.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.ToolTip tipGeneric;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtBigMessage;
        private System.Windows.Forms.ContextMenuStrip cmsSave;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem homeProjectToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dtpExample;
        private System.Windows.Forms.Button btnGetDate;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}

