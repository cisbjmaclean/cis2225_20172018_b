/* Program:         Ch13AnimatedGif
 * Date:            June 2009
 * Programmer:      Bradley/Millspaugh
 * Description:     Demonstrate animation that displays an animated gif file.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ch13AnimatedGif
{
    public partial class AnimationForm : Form
    {
        public AnimationForm()
        {
            InitializeComponent();
        }
    }
}