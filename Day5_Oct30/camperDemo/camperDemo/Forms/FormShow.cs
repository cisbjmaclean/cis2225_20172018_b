﻿using camperDemo.BusinessObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace camperDemo.Forms
{
    public partial class FormShow : FormBase
    {
        
        string printExample = "";

        public FormShow()
        {
            InitializeComponent();
        }

        public void FormShow_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public void FormBase_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void FormShow_Load(object sender, EventArgs e)
        {
            loadCampers();
        }

        private void lstBxCampers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void loadCampers()
        {
            lstBxCampers.Items.Clear();
            foreach (Camper camper in Camp.Campers)
            {
                lstBxCampers.Items.Add(camper.ToString());
                printExample += camper.ToString() + "\n";
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printCampers();

        }//end print click



        public void printCampers()
        {
            PrintDocument myPrintDocument = new PrintDocument();
            
            
            PrintDialog printDialog = new PrintDialog();
            
            

            myPrintDocument.PrintPage += delegate(object sender1, PrintPageEventArgs e1)
            {
                Font headingFont = new Font("Arial", 14, FontStyle.Bold);
                e1.Graphics.DrawString("Campers\n", headingFont, Brushes.Black, new PointF());
                e1.Graphics.DrawString(printExample, this.Font, Brushes.Red, new PointF());
                e1.HasMorePages = false;
            };

            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                myPrintDocument.Print();
            }//end if

            


        }

        private void buttonSaveToFile_Click(object sender, EventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Txt | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter write = new StreamWriter(File.Create(save.FileName));

                String list = "";
                foreach (String current in lstBxCampers.Items)
                {
                    list += current;
                }
                write.Write(list);
                write.Dispose();
            }





        }//end printCampers

        /*
        protected void OnPrintPage(PrintPageEventArgs e1)
        {
            e1.Graphics.DrawString(printExample, this.Font, Brushes.Red, new PointF());
            e1.HasMorePages = false;
        }
        */

    }//end partial class
}//end namespace
