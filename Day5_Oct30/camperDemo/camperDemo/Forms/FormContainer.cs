﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using camperDemo.Forms;
using camperDemo.BusinessObjects;

namespace camperDemo.Forms
{
    public partial class FormContainer : Form
    {

        private FormShow listForm = new FormShow();
        private FormMain addForm = new FormMain();
        private FormShowGraphic showGraphicForm = new FormShowGraphic();
        private static Boolean wantToClose = false;

        public FormContainer()
        {
            InitializeComponent();
        }



        private void FormContainer_Load(object sender, EventArgs e)
        {
            // Set the Parent Form of the Child window.  
            listForm.MdiParent = this;
            addForm.MdiParent = this;

            // Display the new form.  
            listForm.Show();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms.OfType<FormMain>().Count() == 0)
            {
                addForm = new FormMain();
                addForm.MdiParent = this;
            }

            addForm.Show();
            addForm.BringToFront();

        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<FormShow>().Count() == 0)
            {
                listForm = new FormShow();
                listForm.MdiParent = this;

            }
            listForm.loadCampers();
            listForm.Show();
            listForm.BringToFront();

        }

        private void showGraphicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<FormShowGraphic>().Count() == 0)
            {
                showGraphicForm = new FormShowGraphic();
                showGraphicForm.MdiParent = this;

            }
            showGraphicForm.Show();
            showGraphicForm.BringToFront();
        }

        private void loadCampersFromFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormLoadRTF rtfForm = new FormLoadRTF();
            rtfForm.MdiParent = this;
            rtfForm.Show();
        }

        private void FormContainer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Add a confirmation here.
            if (!wantToClose)
            {
                DialogResult close = MessageBox.Show("Close window?", "This will shut down the application, are you sure you want to close?", MessageBoxButtons.YesNo);
                if (close == DialogResult.Yes)
                {
                    wantToClose = true;
                    Utility.ConfirmClose = true;
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;

                }
            }
        }

    }
}


