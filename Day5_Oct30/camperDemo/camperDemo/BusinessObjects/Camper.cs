﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camperDemo.BusinessObjects 
{
    partial class Camper : Person 
    {
    
        private int skillLevel;

        public Camper(String name, String dob, int skillLevel){

            Name = name;
            Dob = dob;
            this.skillLevel = skillLevel;

    }
        public int SkillLevel
        {
            get { return skillLevel; }
            set { skillLevel = value; }
        }

        public override string ToString()
        {
            return "Person: " + Name + ", " + Dob + ", " + skillLevel+"\n";
        }

    }
}
