﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camperDemo.BusinessObjects
{
    class Person
    {
        private String name;
        private String dob;

        public String Dob
        {
            get { return dob; }
            set { dob = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
