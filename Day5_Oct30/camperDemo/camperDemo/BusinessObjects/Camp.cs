﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camperDemo.BusinessObjects
{
    class Camp
    {
        static List<Camper> campers = new List<Camper>();

        public static List<Camper> Campers
        {
            get { return campers; }
            set { campers = value; }
        }
    
    }
}
