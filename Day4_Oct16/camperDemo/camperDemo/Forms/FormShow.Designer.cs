﻿namespace camperDemo.Forms
{
    partial class FormShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstBxCampers = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lstBxCampers
            // 
            this.lstBxCampers.FormattingEnabled = true;
            this.lstBxCampers.Location = new System.Drawing.Point(12, 12);
            this.lstBxCampers.Name = "lstBxCampers";
            this.lstBxCampers.Size = new System.Drawing.Size(464, 277);
            this.lstBxCampers.TabIndex = 0;
            this.lstBxCampers.SelectedIndexChanged += new System.EventHandler(this.lstBxCampers_SelectedIndexChanged);
            // 
            // FormShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 322);
            this.Controls.Add(this.lstBxCampers);
            this.Name = "FormShow";
            this.Text = "FormShow";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormShow_FormClosing);
            this.Load += new System.EventHandler(this.FormShow_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstBxCampers;
    }
}