﻿using camperDemo.BusinessObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace camperDemo.Forms
{
    public partial class FormShow : FormBase
    {
        public FormShow()
        {
            InitializeComponent();
        }

        public void FormShow_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public void FormBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void FormShow_Load(object sender, EventArgs e)
        {
            

            foreach(Camper camper in Camp.Campers)
            {
                lstBxCampers.Items.Add(camper.ToString());
            }
        }

        private void lstBxCampers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
