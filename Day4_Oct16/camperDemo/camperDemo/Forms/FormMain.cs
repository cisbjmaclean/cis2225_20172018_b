﻿using camperDemo.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using camperDemo.BusinessObjects;

namespace camperDemo
{
    public partial class FormMain : FormBase
    {


        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {


        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FormShow formShow = new FormShow();
            //formShow.
            formShow.Show();

        }

        private void txtBxName_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtBxName_Validating(object sender, CancelEventArgs e)
        {
            validateName();
        }

        private bool validateName()
        {
            errorProvider1.SetError(txtBxName, "");
            Console.WriteLine("validating triggered");
            bool valid = true;
            if (txtBxName.Text == String.Empty)
            {
                //e.Cancel = true;
                valid = false;
                errorProvider1.SetError(txtBxName, "Name is required");
            }
            else
            {
                if (!txtBxName.Text.Contains(" "))
                {
                    valid = false;
                    errorProvider1.SetError(txtBxName, "First and last name required");
                }
            }
            return valid;
        }

        private void mskdTxtBxDOB_Validating(object sender, CancelEventArgs e)
        {

            validateDOB();


        }

        private void radBtnBeginner_Validating(object sender, CancelEventArgs e)
        {
            //MessageBox.Show("Validating radio buttons");
        }

        private bool radioButtonValidate()
        {
            if (radBtnAdvanced.Checked || radBtnBeginner.Checked || radBtnIntermediate.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void grpBxSkillLevel_Validating(object sender, CancelEventArgs e)
        {
            //MessageBox.Show("Validating radio buttons on groupbox");
            if (!radioButtonValidate())
            {
                errorProvider1.SetError(grpBxSkillLevel, "Skill Level Required");
            }
            else
            {
                errorProvider1.SetError(grpBxSkillLevel, "");
            }
        }

        private bool validateDOB()
        {
            string text = mskdTxtBxDOB.Text;
            DateTime parsed;


            bool valid = true;
            if (DateTime.TryParseExact(text, "yyyy-MM-dd",
                                                    CultureInfo.InvariantCulture,
                                                    DateTimeStyles.None,
                                                    out parsed) == true)
            {
                errorProvider1.SetError(mskdTxtBxDOB, "");
            }
            else
            {
                valid = false;
                errorProvider1.SetError(mskdTxtBxDOB, "Must be in yyyy-MM-dd format");
            }
            return valid;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool valid = true;
            if (!radioButtonValidate())
            {
                valid = false;
                errorProvider1.SetError(grpBxSkillLevel, "Skill Level Required");
            }
            else
            {
                errorProvider1.SetError(grpBxSkillLevel, "");

            }

            valid = valid && validateDOB();

            valid = valid && validateName();

            if (valid)
            {

                int skillLevel = 0;
                if (radBtnBeginner.Checked)
                {
                    skillLevel = 1;
                }
                if (radBtnIntermediate.Checked)
                {
                    skillLevel = 2;
                }
                if (radBtnAdvanced.Checked)
                {
                    skillLevel = 3;
                }


                //Shouldn't save the camper unless we fully validate the form
                Camper newCamper = new Camper(txtBxName.Text, mskdTxtBxDOB.Text, skillLevel);
                Camp.Campers.Add(newCamper);
            }
        }

        private void radBtnBeginner_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
