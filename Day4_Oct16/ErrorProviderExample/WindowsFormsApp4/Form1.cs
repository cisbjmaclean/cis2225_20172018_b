﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }



        private void maskedTextBoxPhone_Validating(object sender, CancelEventArgs e)
        {
            Console.WriteLine("validating triggered for maskedTextBx");
            
        }
        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("FormClosing_1 is triggered");
            e.Cancel = false;
        }

        private void Form1_Leave(object sender, EventArgs e)
        {

        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            MessageBox.Show("text changed in name textbox");
        }


        private void textBoxName_Leave(object sender, EventArgs e)
        {
            MessageBox.Show("left name textbox");
        }

        private void textBoxName_Validating_1(object sender, CancelEventArgs e)
        {
            errorProviderForm1.SetError(textBoxName, "");
            Console.WriteLine("validating triggered");

            if (textBoxName.Text == String.Empty)
            {
                //e.Cancel = true;
                errorProviderForm1.SetError(textBoxName, "Name is required");
            }

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {

        }
    }
}
