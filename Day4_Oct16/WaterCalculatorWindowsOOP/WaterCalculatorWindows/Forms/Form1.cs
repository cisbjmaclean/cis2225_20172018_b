﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterCalculatorWindows.BusinessObjects;

namespace WaterCalculatorWindows
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void calculate()
        {
            double diameter;
            if (!double.TryParse(textBoxDiameter.Text, out diameter))
            {
                //
            }

            double height;
            if (!double.TryParse(textBoxHeight.Text, out height))
            {
            }

            //Calculate the volume
            double volume = Math.PI * Math.Pow((diameter / 2.0), 2) * (height);


            double flowrate;
            if (!double.TryParse(textBoxFlowRate.Text, out flowrate))
            {
            }

            WaterCalculator calculator = new WaterCalculator(diameter, flowrate, height);
            
            //Set the output.
            textBoxTime.Text = Convert.ToString(calculator.CalculateTime());
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

    }
}
