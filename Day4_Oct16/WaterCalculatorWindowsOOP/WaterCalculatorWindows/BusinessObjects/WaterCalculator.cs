﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCalculatorWindows.BusinessObjects
{
    class WaterCalculator
    {

        private double diameter;
        private double flowRate;
        private double height;

        public const double CUBIC_INCHES_PER_LITER = 61.0;

        public double Diameter { get { return diameter; } set { diameter = value; } }
        public double FlowRate { get { return flowRate; } set { flowRate = value; } }
        public double Height { get { return height; } set { height = value; } }

        public WaterCalculator()
        {

        }

        public WaterCalculator(double diameter, double flowRate, double height)
        {
            this.diameter = diameter;
            this.flowRate = flowRate;
            this.height = height;
        }

        public double CalculateTime()
        {

            double volume = Math.PI * Math.Pow((diameter / 2.0), 2) * (height);

            double time = volume * (flowRate / CUBIC_INCHES_PER_LITER);

            return time;
        }

        public string ToString()
        {
            return "it will take " + CalculateTime() + " seconds";
        }


    }
}
