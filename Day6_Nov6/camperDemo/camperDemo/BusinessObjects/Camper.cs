﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using camperDemo.BusinessObjects;

namespace camperDemo.BusinessObjects 
{
    public class Camper : Person 
    {
    
        private dynamic skillLevel;
        
        public Camper(String name, String dob, int skillLevel){

            var middleName = "John";
            

            Name = name;
            Dob = dob;
            this.skillLevel = skillLevel;
            this.skillLevel = "A";
            this.skillLevel = 1;
    }
        public int SkillLevel
        {
            get { return skillLevel; }
            set { skillLevel = value; }
        }

        public override string ToString()
        {
            return base.ToString() +" Skill: " + skillLevel+"\n";
        }

        public override int getAge()
        {
            return 10;
        }

    }
}
