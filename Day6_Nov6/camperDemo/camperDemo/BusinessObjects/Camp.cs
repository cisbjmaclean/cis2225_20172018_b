﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camperDemo.BusinessObjects
{
    class Camp<T>
    {
        static List<T> campers = new List<T>();

        public static List<T> Campers
        {
            get { return campers; }
            set { campers = value; }
        }
    
    }
}
