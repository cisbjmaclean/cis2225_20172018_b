﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camperDemo.BusinessObjects
{
    public abstract class Person
    {
        private String name;
        private String dob;

        public abstract int getAge();

        public String Dob
        {
            get { return dob; }
            set { dob = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public override String ToString()
        {
            return "Name: " + name + " DOB: " + dob;
        }
    }
}
