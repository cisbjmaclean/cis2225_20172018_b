﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using camperDemo.BusinessObjects;

namespace camperDemo.Forms
{
    public partial class FormShowGraphic : Form
    {

        private int x = 10;
        private int y = 10;

        private void randomPaint(int numberOfTimes)
        {
            Random r = new Random();
            Color rC;
            SolidBrush b1;



            Graphics g = this.CreateGraphics();

            for (int i = 0; i < numberOfTimes; i++)
            {
                rC = Color.FromArgb(r.Next(255), r.Next(255), r.Next(255));
                b1 = new SolidBrush(rC);
                g.FillEllipse(b1, x, y, 30, 30);
                x += 40;
                y += 40;
            }
        }
        public FormShowGraphic()
        {
            InitializeComponent();
        }

        private void FormShowGraphic_Load(object sender, EventArgs e)
        {
        }




        private void btnDraw_Click_1(object sender, EventArgs e)
        {
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(
                50, 100, 150, 150);
            graphics.DrawEllipse(System.Drawing.Pens.Black, rectangle);
            //graphics.DrawRectangle(System.Drawing.Pens.Red, rectangle);

            int numberOfCampers = camperDemo.BusinessObjects.Camp<Camper>.Campers.Count();

            randomPaint(numberOfCampers);
        }

        private void FormShowGraphic_Paint_1(object sender, PaintEventArgs e)
        {
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(
                50, 100, 150, 150);
            graphics.DrawEllipse(System.Drawing.Pens.Black, rectangle);
            //graphics.DrawRectangle(System.Drawing.Pens.Red, rectangle);

            int numberOfCampers = camperDemo.BusinessObjects.Camp<Camper>.Campers.Count();

            randomPaint(numberOfCampers);

        }
    }

}
