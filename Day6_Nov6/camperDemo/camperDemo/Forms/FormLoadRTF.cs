﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace camperDemo.Forms
{
    public partial class FormLoadRTF : Form
    {
        public FormLoadRTF()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormLoadRTF_Load(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
                        ofd.Filter = "Txt | *.txt";
                        if (ofd.ShowDialog() == DialogResult.OK)
                        {
                            StreamReader sr = new StreamReader(File.OpenRead(ofd.FileName));
                            String contents = sr.ReadToEnd();
                            richTextBox1.Text = contents;
                        }
        }
    }
}
