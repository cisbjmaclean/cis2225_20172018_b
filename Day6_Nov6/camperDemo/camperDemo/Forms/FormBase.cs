﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using camperDemo.BusinessObjects;


namespace camperDemo
{
    public partial class FormBase : Form
    {
        public FormBase()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            double testDouble = Decimal.ToDouble(123m);
            Console.WriteLine("double=" + testDouble);
        }

        private void FormBase_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
