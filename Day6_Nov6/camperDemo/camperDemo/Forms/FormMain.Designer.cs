﻿namespace camperDemo
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtBxName = new System.Windows.Forms.TextBox();
            this.mskdTxtBxDOB = new System.Windows.Forms.MaskedTextBox();
            this.radBtnBeginner = new System.Windows.Forms.RadioButton();
            this.radBtnIntermediate = new System.Windows.Forms.RadioButton();
            this.radBtnAdvanced = new System.Windows.Forms.RadioButton();
            this.grpBxSkillLevel = new System.Windows.Forms.GroupBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnShowGraphic = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.grpBxSkillLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBxName
            // 
            this.txtBxName.Location = new System.Drawing.Point(92, 30);
            this.txtBxName.Name = "txtBxName";
            this.txtBxName.Size = new System.Drawing.Size(100, 20);
            this.txtBxName.TabIndex = 0;
            this.txtBxName.TextChanged += new System.EventHandler(this.txtBxName_TextChanged);
            this.txtBxName.Validating += new System.ComponentModel.CancelEventHandler(this.txtBxName_Validating);
            // 
            // mskdTxtBxDOB
            // 
            this.mskdTxtBxDOB.Location = new System.Drawing.Point(92, 56);
            this.mskdTxtBxDOB.Name = "mskdTxtBxDOB";
            this.mskdTxtBxDOB.Size = new System.Drawing.Size(100, 20);
            this.mskdTxtBxDOB.TabIndex = 3;
            this.mskdTxtBxDOB.Validating += new System.ComponentModel.CancelEventHandler(this.mskdTxtBxDOB_Validating);
            // 
            // radBtnBeginner
            // 
            this.radBtnBeginner.AutoSize = true;
            this.radBtnBeginner.Location = new System.Drawing.Point(30, 19);
            this.radBtnBeginner.Name = "radBtnBeginner";
            this.radBtnBeginner.Size = new System.Drawing.Size(67, 17);
            this.radBtnBeginner.TabIndex = 4;
            this.radBtnBeginner.TabStop = true;
            this.radBtnBeginner.Text = "Beginner";
            this.radBtnBeginner.UseVisualStyleBackColor = true;
            this.radBtnBeginner.CheckedChanged += new System.EventHandler(this.radBtnBeginner_CheckedChanged);
            this.radBtnBeginner.Validating += new System.ComponentModel.CancelEventHandler(this.radBtnBeginner_Validating);
            // 
            // radBtnIntermediate
            // 
            this.radBtnIntermediate.AutoSize = true;
            this.radBtnIntermediate.Location = new System.Drawing.Point(30, 42);
            this.radBtnIntermediate.Name = "radBtnIntermediate";
            this.radBtnIntermediate.Size = new System.Drawing.Size(83, 17);
            this.radBtnIntermediate.TabIndex = 5;
            this.radBtnIntermediate.TabStop = true;
            this.radBtnIntermediate.Text = "Intermediate";
            this.radBtnIntermediate.UseVisualStyleBackColor = true;
            // 
            // radBtnAdvanced
            // 
            this.radBtnAdvanced.AutoSize = true;
            this.radBtnAdvanced.Location = new System.Drawing.Point(30, 65);
            this.radBtnAdvanced.Name = "radBtnAdvanced";
            this.radBtnAdvanced.Size = new System.Drawing.Size(74, 17);
            this.radBtnAdvanced.TabIndex = 6;
            this.radBtnAdvanced.TabStop = true;
            this.radBtnAdvanced.Text = "Advanced";
            this.radBtnAdvanced.UseVisualStyleBackColor = true;
            // 
            // grpBxSkillLevel
            // 
            this.grpBxSkillLevel.Controls.Add(this.radBtnBeginner);
            this.grpBxSkillLevel.Controls.Add(this.radBtnAdvanced);
            this.grpBxSkillLevel.Controls.Add(this.radBtnIntermediate);
            this.grpBxSkillLevel.Location = new System.Drawing.Point(92, 120);
            this.grpBxSkillLevel.Name = "grpBxSkillLevel";
            this.grpBxSkillLevel.Size = new System.Drawing.Size(143, 93);
            this.grpBxSkillLevel.TabIndex = 7;
            this.grpBxSkillLevel.TabStop = false;
            this.grpBxSkillLevel.Text = "Skill Level";
            this.toolTip1.SetToolTip(this.grpBxSkillLevel, "How good are they at volleyball?");
            this.grpBxSkillLevel.Validating += new System.ComponentModel.CancelEventHandler(this.grpBxSkillLevel_Validating);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(18, 33);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(18, 59);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(30, 13);
            this.lblDOB.TabIndex = 10;
            this.lblDOB.Text = "DOB";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(92, 261);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.toolTip1.SetToolTip(this.btnSave, "Save the camper");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnShowGraphic
            // 
            this.btnShowGraphic.Location = new System.Drawing.Point(343, 261);
            this.btnShowGraphic.Margin = new System.Windows.Forms.Padding(1);
            this.btnShowGraphic.Name = "btnShowGraphic";
            this.btnShowGraphic.Size = new System.Drawing.Size(69, 36);
            this.btnShowGraphic.TabIndex = 13;
            this.btnShowGraphic.Text = "Show Graphic";
            this.btnShowGraphic.UseVisualStyleBackColor = true;
            this.btnShowGraphic.Click += new System.EventHandler(this.btnShowGraphic_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 326);
            this.Controls.Add(this.btnShowGraphic);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblDOB);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.grpBxSkillLevel);
            this.Controls.Add(this.mskdTxtBxDOB);
            this.Controls.Add(this.txtBxName);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.grpBxSkillLevel.ResumeLayout(false);
            this.grpBxSkillLevel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBxName;
        private System.Windows.Forms.MaskedTextBox mskdTxtBxDOB;
        private System.Windows.Forms.RadioButton radBtnBeginner;
        private System.Windows.Forms.RadioButton radBtnIntermediate;
        private System.Windows.Forms.RadioButton radBtnAdvanced;
        private System.Windows.Forms.GroupBox grpBxSkillLevel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnShowGraphic;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
    }
}